<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Stockcard extends REST_Controller {

  function __construct($config = 'rest') {
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    $method = $_SERVER['REQUEST_METHOD'];
    if($method == "OPTIONS") {
        die();
    }
      parent::__construct($config);
      $this->load->database();
  }

    //Menampilkan data
    function index_get() {
        $keyword = $this->get('keyword');
        $default = "status_code !=0 ";
		    $where = "(car_name like '%".$keyword."%' OR license_plate like '%".$keyword."%' OR car_brand like '%".$keyword."%' OR car_color like '%".$keyword."%') AND status_code != 0";
        if ($keyword == '') {
            $this->db->where($default);
            $this->db->order_by('last_update_date', "desc");
            $kontak = $this->db->get('stockcard')->result();
        } else {
            $this->db->where($where);
            $this->db->order_by('last_update_date', "desc");
            $kontak = $this->db->get('stockcard')->result();
        }
        $this->response($kontak, 200);
    }

    function mobile_get() {
        $keyword = $this->get('keyword');
        $default = "status_code = 1 ";
		    $where = "(car_name like '%".$keyword."%' OR license_plate like '%".$keyword."%' OR car_brand like '%".$keyword."%' OR car_color like '%".$keyword."%') AND status_code != 0";
        if ($keyword == '') {
            $this->db->where($default);
            $this->db->order_by('last_update_date');
            $kontak = $this->db->get('stockcard_hpp')->result();
        } else {
            $this->db->where($where);
            $this->db->order_by('last_update_date');
            $kontak = $this->db->get('stockcard_hpp')->result();
        }
        $this->response($kontak, 200);
    }

    //Menampilkan data
    function active_get() {
        $keyword = $this->get('keyword');
        $default = "status_code =1 ";
		    $where = "(car_name like '%".$keyword."%' OR license_plate like '%".$keyword."%' OR car_brand like '%".$keyword."%' OR car_color like '%".$keyword."%') AND status_code != 0";
        if ($keyword == '') {
            $this->db->where($default);
            $kontak = $this->db->get('stockcard')->result();
        } else {
            $this->db->where($where);
            $kontak = $this->db->get('stockcard')->result();
        }
        $this->response($kontak, 200);
    }

    // insert new data
    function index_post() {
        $data = array(
            'license_plate'           => $this->post('license_plate'),
            'car_name'          => $this->post('car_name'),
            'car_brand'          => $this->post('car_brand'),
			'car_type'          => $this->post('car_type'),
			'tahun'          => $this->post('tahun'),
			'stnk_no'          => $this->post('stnk_no'),
			'car_fuel'          => $this->post('car_fuel'),
			'purchase_date'          => $this->post('purchase_date'),
			'purchaser_name'          => $this->post('purchaser_name'),
			'purchase_price'          => $this->post('purchase_price'),
            'car_color'          => $this->post('car_color'),
            'car_transmision'          => $this->post('car_transmision'),
			'last_update_date'          => $this->post('last_update_date'),
          'status_name'          => $this->post('status_name'),
          'status_code'          => $this->post('status_code'),
          'updated_by'          => $this->post('updated_by'));
          
        $insert = $this->db->insert('stockcard', $data);
        if ($insert) {
            $images = $this->post('car_images');
            foreach ($images as $key => $value) {
                $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $value));
                $image_name = md5(uniqid(rand(), true));
                $filename = $image_name . '.' . 'png';
                //rename file name with random number
                $path = 'uploads/images/';
        
                //image uploading folder path
                file_put_contents($path.$filename, $image);
                $val = intval($key) + 1;
                $data = array(
                    'car_images_'.$val           => $filename );
                $this->db->where('license_plate', $this->post('license_plate'));
                $update = $this->db->update('stockcard', $data);
            }
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    // update data
    function index_put() {
        $id = $this->put('id');
        $data = array(
                    'license_plate'           => $this->put('license_plate'),
                    'car_brand'          => $this->put('car_brand'),
                    'car_name'          => $this->put('car_name'),
					'car_type'          => $this->put('car_type'),
					'tahun'          => $this->put('tahun'),
					'stnk_no'          => $this->put('stnk_no'),
					'car_fuel'          => $this->put('car_fuel'),
					'purchase_date'          => $this->put('purchase_date'),
					'purchaser_name'          => $this->put('purchaser_name'),
					'purchase_price'          => $this->put('purchase_price'),
					'car_transmision'          => $this->put('car_transmision'),
                    'car_color'          => $this->put('car_color'),
					'last_update_date'          => $this->put('last_update_date'),
          'status_code'         => $this->put('status_code'),
          'status_name'         => $this->put('status_name'),
					'updated_by'          => $this->put('updated_by'));
        $this->db->where('id', $id);
        $update = $this->db->update('stockcard', $data);
        if ($update) {
            $images = $this->put('car_images');
            if (sizeof($images) > 0) {
                $data = array(
                    'car_images_1' => null,
                    'car_images_2' => null,
                    'car_images_3' => null,
                    'car_images_4' => null,
                    'car_images_5' => null,
                 );
                $this->db->where('id', $id);
                $update = $this->db->update('stockcard', $data);
            }
            foreach ($images as $key => $value) {
                $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $value));
                $image_name = md5(uniqid(rand(), true));
                $filename = $image_name . '.' . 'png';
                //rename file name with random number
                $path = 'uploads/images/';
        
                //image uploading folder path
                file_put_contents($path.$filename, $image);
                $val = intval($key) + 1;
                $data = array(
                    'car_images_'.$val           => $filename );
                $this->db->where('id', $id);
                $update = $this->db->update('stockcard', $data);
            }
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    // delete
    function index_delete() {
        $id = $this->delete('id');
        $this->db->where('id', $id);
        $delete = $this->db->delete('stockcard');
        if ($delete) {
            $this->response(array('status' => 'success'), 201);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
}
?>
