<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Systable extends REST_Controller {

  function __construct($config = 'rest') {
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    $method = $_SERVER['REQUEST_METHOD'];
    if($method == "OPTIONS") {
        die();
    }
      parent::__construct($config);
      $this->load->database();
  }

    //Menampilkan data
    function index_get() {
        $syscode = $this->get('syscode');
        if ($syscode == '') {
            $kontak = $this->db->get('ms_systable')->result();
        } else {
            $this->db->where('syscode', $syscode);
            $kontak = $this->db->get('ms_systable')->result();
        }
        $this->response($kontak, 200);
    }

    // insert new data
    function index_post() {
        $data = array(
                    'syscode'           => $this->post('syscode'),
                    'code'          => $this->post('code'),
					'description1'          => $this->post('description1'),
					'description2'          => $this->post('description2'),
                    'label'          => $this->post('label'));
        $insert = $this->db->insert('ms_systable', $data);
        if ($insert) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    // update data
    function index_put() {
        $id = $this->put('id');
        $data = array(
					'id' => $this->put('id'),
                    'syscode'           => $this->put('syscode'),
                    'code'          => $this->put('code'),
					'description1'          => $this->put('description1'),
					'description2'          => $this->put('description2'),
                    'label'          => $this->put('label'));
        $this->db->where('id', $id);
        $update = $this->db->update('ms_systable', $data);
        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    // delete
    function index_delete() {
        $id = $this->delete('id');
        $this->db->where('id', $id);
        $delete = $this->db->delete('ms_systable');
        if ($delete) {
            $this->response(array('status' => 'success'), 201);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
}
?>
