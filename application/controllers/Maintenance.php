<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Maintenance extends REST_Controller {

  function __construct($config = 'rest') {
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    $method = $_SERVER['REQUEST_METHOD'];
    if($method == "OPTIONS") {
        die();
    }
      parent::__construct($config);
      $this->load->database();
  }

    //Menampilkan data
    function index_get() {
        $keyword = $this->get('stockcard_id');
        $default = "status_code !=0 ";
		    $where = "stockcard_id = ".$keyword." and ".$default;
        if ($keyword == '') {
            $this->db->where($default);
            $kontak = $this->db->get('maintenance')->result();
        } else {
            $this->db->where($where);
            $kontak = $this->db->get('maintenance')->result();
        }
        $this->response($kontak, 200);
    }

    // insert new data
    function index_post() {
        $data = array(
                    'stockcard_id'           => $this->post('stockcard_id'),
                    'maintenance_date'          => $this->post('maintenance_date'),
                    'maintenance_name'          => $this->post('maintenance_name'),
					'description1'          => $this->post('description1'),
					'description2'          => $this->post('description2'),
					'maintenance_cost'          => $this->post('maintenance_cost'),
					'updated_by'          => $this->post('updated_by'),
					'last_update_date'          => $this->post('last_update_date'),
					'operator_name'          => $this->post('operator_name'),
					'status_code'          => $this->post('status_code'),
					'status_name'          => $this->post('status_name'));
	    
		log_message('error', $this->post('maintenance_date'));
        $insert = $this->db->insert('maintenance', $data);
        if ($insert) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    // update data
    function index_put() {
        $id = $this->put('id');
        $data = array(
          'stockcard_id'           => $this->put('stockcard_id'),
          'maintenance_date'          => $this->put('maintenance_date'),
          'maintenance_name'          => $this->put('maintenance_name'),
          'description1'          => $this->put('description1'),
          'description2'          => $this->put('description2'),
          'maintenance_cost'          => $this->put('maintenance_cost'),
          'updated_by'          => $this->put('updated_by'),
          'last_update_date'          => $this->put('last_update_date'),
          'operator_name'          => $this->put('operator_name'),
          'status_code'          => $this->put('status_code'),
          'status_name'          => $this->put('status_name'));
        $this->db->where('id', $id);
        $update = $this->db->update('maintenance', $data);
        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    // delete
    function index_delete() {
        $id = $this->delete('id');
        $this->db->where('id', $id);
        $delete = $this->db->delete('maintenance');
        if ($delete) {
            $this->response(array('status' => 'success'), 201);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
}
?>
