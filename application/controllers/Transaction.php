<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Transaction extends REST_Controller {

  function __construct($config = 'rest') {
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    $method = $_SERVER['REQUEST_METHOD'];
    if($method == "OPTIONS") {
        die();
    }
      parent::__construct($config);
      $this->load->database();
  }

    //Menampilkan data
    function index_get() {
        $keyword = $this->get('keyword');
        $default = "status_code !=0 ";
        if($this->get('date_from')!=null && $this->get('date_from')!=""){
          $default = $default." and transaction_date >= '".$this->get('date_from')."' ";
        }
        if($this->get('date_to')!=null && $this->get('date_to')!=""){
          $default = $default." and transaction_date <= '".$this->get('date_to')."' ";
        }
		    $where = "(transaction_no like '%".$keyword."%' OR customer_name like '%".$keyword."%' OR car_name like '%".$keyword."%' OR license_plate like '%".$keyword."%' OR car_brand like '%".$keyword."%' OR car_color like '%".$keyword."%') AND status_code != 0";
        if ($keyword == '') {
            $this->db->where($default);
            $kontak = $this->db->get('t_transaction')->result();
        } else {
            $this->db->where($where);
            $kontak = $this->db->get('t_transaction')->result();
        }
        $this->response($kontak, 200);
    }

    // insert new data
    function index_post() {
        $config = [
            'mailtype'  => 'html',
            'charset'   => 'utf-8',
            'protocol'  => 'smtp',
            'smtp_host' => 'mail.dianmobil.com',
            'smtp_user' => 'admin@dianmobil.com',  // Email gmail
            'smtp_pass'   => 'S0toayam',  // Password gmail
            'smtp_crypto' => 'ssl',
            'smtp_port'   => 465,
            'crlf'    => "\r\n",
            'newline' => "\r\n"
        ];

        // Load library email dan konfigurasinya
        $this->load->library('email', $config);

        // Email dan nama pengirim
        $this->email->from('admin@dianmobil.com', 'Admin Dian Mobil');

        // Email penerima
        $this->email->to('doni_dm@yahoo.com'); // Ganti dengan email tujuan

        // Lampiran email, isi dengan url/path file
        //$this->email->attach('https://masrud.com/content/images/20181215150137-codeigniter-smtp-gmail.png');

        // Subject email
        $this->email->subject('Transaksi Baru');

        // Isi email
        $this->email->message("Dear Pak Haji, pada tanggal ".$this->post('transaction_date')." berikut ada transaksi mobil baru.<br><br>Mobil ".$this->post('car_brand')." ".$this->post('car_color')." dengan plat nomor ".$this->post('license_plate')." Terjual senilai Rp ".number_format($this->post('total_amount'),2,",",".")."<br><br> Terima kasih.");

      $query = $this->db->query("select CONCAT('INV', DATE_FORMAT('".$this->post("transaction_date")."','%y%m%d'),
               (select LPAD(
                   (select case when RIGHT(MAX(transaction_no),4) is null then 1 else CAST(RIGHT(MAX(transaction_no),4) as UNSIGNED)+1 end from t_transaction),4,'0'))) as transaction_no
               from dual");
      $row = $query->row();
       $data = array(
        'transaction_date'           => $this->post('transaction_date'),
        'transaction_no'        => $row->transaction_no,
        'stockcard_id'          => $this->post('stockcard_id'),
        'machine_no'          => $this->post('machine_no'),
        'bpkb_no'          => $this->post('bpkb_no'),
        'stnk_no'          => $this->post('stnk_no'),
        'notes1'          => $this->post('notes1'),
        'notes2'          => $this->post('notes2'),
        'customer_name'          => $this->post('customer_name'),
        'customer_address'          => $this->post('customer_address'),
        'total_amount'          => $this->post('total_amount'),
        'car_brand'          => $this->post('car_brand'),
        'car_color'          => $this->post('car_color'),
        'tahun'          => $this->post('tahun'),
        'license_plate'          => $this->post('license_plate'),
        'car_name'          => $this->post('car_name'),
        'commision'          => $this->post('commision'),
        'sales_name'          => $this->post('sales_name'),
  		'last_update_date'          => $this->post('last_update_date'),
        'status_name'          => $this->post('status_name'),
        'status_code'          => $this->post('status_code'),
  		'updated_by'          => $this->post('updated_by'));
        $insert = $this->db->insert('t_transaction', $data);
        if ($insert) {
            $this->response($data, 200);
            // Tampilkan pesan sukses atau error
            if ($this->email->send()) {
                // echo 'Sukses! email berhasil dikirim.';
            } else {
                // echo 'Error! email tidak dapat dikirim.';
            }
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    // update data
    function index_put() {
        $id = $this->put('id');
        $data = array(
          'transaction_date'           => $this->put('transaction_date'),
          'stockcard_id'          => $this->put('stockcard_id'),
          'machine_no'          => $this->put('machine_no'),
          'bpkb_no'          => $this->put('bpkb_no'),
          'stnk_no'          => $this->put('stnk_no'),
   		          'notes1'          => $this->put('notes1'),
   		          'notes2'          => $this->put('notes2'),
          'customer_name'          => $this->put('customer_name'),
          'customer_address'          => $this->put('customer_address'),
          'total_amount'          => $this->put('total_amount'),
          'car_brand'          => $this->put('car_brand'),
          'car_color'          => $this->put('car_color'),
          'tahun'          => $this->put('tahun'),
          'license_plate'          => $this->put('license_plate'),
          'sales_name'          => $this->put('sales_name'),
          'car_name'          => $this->put('car_name'),
         'commision'          => $this->put('commision'),
          'last_update_date'          => $this->put('last_update_date'),
          'status_name'          => $this->put('status_name'),
          'status_code'          => $this->put('status_code'),
          'updated_by'          => $this->put('updated_by'));
        $this->db->where('id', $id);
        $update = $this->db->update('t_transaction', $data);
        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    // delete
    function index_delete() {
        $id = $this->delete('id');
        $this->db->where('id', $id);
        $delete = $this->db->delete('t_transaction');
        if ($delete) {
            $this->response(array('status' => 'success'), 201);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
}
?>
