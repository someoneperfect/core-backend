<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Report extends REST_Controller {

  function __construct($config = 'rest') {
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    $method = $_SERVER['REQUEST_METHOD'];
    if($method == "OPTIONS") {
        die();
    }
      parent::__construct($config);
      $this->load->database();
  }

    //Menampilkan data
    function labarugi_get() {
        $keyword = $this->get('keyword');
        $default = "status_code !=0 ";
        if($this->get('date_from')!=null && $this->get('date_from')!=""){
          $default = $default." and transaction_date >= '".$this->get('date_from')."' ";
        }
        if($this->get('date_to')!=null && $this->get('date_to')!=""){
          $default = $default." and transaction_date <= '".$this->get('date_to')."' ";
        }
        $where = "(customer_name like '%".$keyword."%' OR car_name like '%".$keyword."%' OR license_plate like '%".$keyword."%' OR car_brand like '%".$keyword."%' OR car_color like '%".$keyword."%')";
        if ($keyword == '') {
            $this->db->where($default);
            $kontak = $this->db->get('labarugi')->result();
        } else {
            $this->db->where($default);
            $kontak = $this->db->get('labarugi')->result();
        }
        $this->response($kontak, 200);
    }
}
?>
