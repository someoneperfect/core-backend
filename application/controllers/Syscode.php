<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Syscode extends REST_Controller {

  function __construct($config = 'rest') {
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    $method = $_SERVER['REQUEST_METHOD'];
    if($method == "OPTIONS") {
        die();
    }
      parent::__construct($config);
      $this->load->database();
  }

    //Menampilkan data
    function index_get() {
        $code = $this->get('code');
        if ($code == '') {
            $kontak = $this->db->get('ms_syscode')->result();
        } else {
            $this->db->where('code', $code);
            $kontak = $this->db->get('ms_syscode')->result();
        }
        $this->response($kontak, 200);
    }

    // insert new data
    function index_post() {
        $data = array(
                    'code'           => $this->post('code'),
                    'label'          => $this->post('label'));
        $insert = $this->db->insert('ms_syscode', $data);
        if ($insert) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    // update data
    function index_put() {
        $code = $this->put('code');
        $data = array(
                    'code'       => $this->put('code'),
                    'label'      => $this->put('label'));
        $this->db->where('code', $code);
        $update = $this->db->update('ms_syscode', $data);
        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    // delete
    function index_delete() {
        $code = $this->delete('code');
        $this->db->where('code', $code);
        $delete = $this->db->delete('ms_syscode');
        if ($delete) {
            $this->response(array('status' => 'success'), 201);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
}
?>
