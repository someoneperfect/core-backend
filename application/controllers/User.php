<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class User extends REST_Controller {

    function __construct($config = 'rest') {
      header('Access-Control-Allow-Origin: *');
      header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
      header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
      $method = $_SERVER['REQUEST_METHOD'];
      if($method == "OPTIONS") {
          die();
      }
        parent::__construct($config);
        $this->load->database();
    }

    //Menampilkan data
    function index_get() {
        $usernm = $this->get('usernm');
        $passwd = $this->get('passwd');
        if ($usernm == '' && $passwd == '') {
            $result = $this->response(array('status' => 'bad request', 400));

        } else {
            $this->db->where('usernm', $usernm)
						->where('passwd', md5($passwd))
						->where('active',1);
            $kontak = $this->db->get('ms_user')->result();

            $result = $this->response($kontak, 200);

        }

    }

    // insert new data
    function index_post() {
        $data = array(
                    'usernm'           => $this->post('usernm'),
                    'passwd'          => md5($this->post('passwd')),
					'role'          => $this->post('role'));
        $insert = $this->db->insert('ms_user', $data);
        if ($insert) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    // update data
    function index_put() {
        $usernm = $this->put('usernm');
        $data = array(
                    'usernm'       => $this->put('usernm'),
                    'passwd'      => $this->put('passwd'),
                    'role'      => $this->put('role'),
                    'active'      => $this->put('active'));
        $this->db->where('usernm', $usernm);
        $update = $this->db->update('ms_user', $data);
        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
}
?>
